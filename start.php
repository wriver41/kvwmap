<?php
# Objekt f�r graphische Benutzeroberfl�che erzeugen
if($_REQUEST['go'] == 'getMap_ajax'){
	$GUI=new GUI_core("map.php","main.css","html");
}
else{
	$GUI=new GUI("map.php","main.css","html");
}

# �bergabe aller Formularvariablen an die Benutzeroberfl�che an formvars
# Dabei wird unterschieden zwischen Aufrufen �ber das Internet oder von der Komandozeile aus
if (is_array($argc) AND $argc[1]!='') {
 # Aufruf des PHP-Skriptes �ber die Komandozeile (CLI)
 # Wenn die Variable argc > 0 ist, wurde die Datei von der Komandozeile aus aufgerufen
 # in dem Fall k�nnen die �bergebenen Parameter hier der formvars-Variable �bergeben werden.
 $arg['go']=$argv[1];
 $arg['ist_Fortfuehrung']=$argv[2];
 $arg['WLDGE_lokal']=$argv[3];
 $arg['WLDGE_Datei_lokal']=$argv[4];
 $GUI->formvars=$arg;
}
else {
  # �bergeben der Variablen aus den Post oder Get Aufrufen
  # normaler Aufruf des PHP-Skriptes �ber Apache oder CGI  
  #$GUI->formvars=stripScript($_REQUEST);
  $GUI->formvars=$_REQUEST;
}

####################################
# �bergeben der Datenbank an die GUI
$GUI->database=$GISdb;

#################################################################################
# Setzen der Konstante, ob in die Datenbank geschrieben werden soll oder nicht.
# Kann z.B. zu Testzwecken ausgeschaltet werden.
if ($GUI->formvars['disableDbWrite']=='1') {
	define('DBWRITE',false);
}
else {
	define('DBWRITE',DEFAULTDBWRITE);
}
if (!DBWRITE) { echo '<br>Das Schreiben in die Datenbank wird unterdr�ckt!'; }

# �ffnen der Datenbankverbindung zur Kartenverwaltung (MySQL)
if (!$GUI->database->open()) {
  # Pr�fen ob eine neue Datenbank angelegt werden soll
  if ($GUI->formvars['go']=='install-mysql-db') {
    # Anlegen der neuen Datenbank
    # Herstellen der Verbindung mit defaultwerten
    $GUI->dbConn=mysql_connect('localhost','kvwmap','kvwmap');
    $GUI->debug->write("MySQL Datenbank mit ID: ".$GUI->dbConn." und Name: mysql ausw�hlen.",4);
    # Ausw�hlen der Datenbank mysql
    mysql_select_db('mysql',$GUI->dbConn);
    # Erzeugen der leeren Datenbank f�r kvwmap
    $sql = 'CREATE DATABASE '.$GUI->database->dbName.' CHARACTER SET latin1 COLLATE latin1_german2_ci';
    $GUI->database->execSQL($sql,4,0);
    # Anlegen der leeren Tabellen f�r kvwmap
    if ($GUI->formvars['install-GUI']) {
      # Demo Daten in Datenbank schreiben
      $sql = file_get_contents(LAYOUTPATH.'sql_dumps/mysql_setup_GUI.sql');     
      $GUI->database->execSQL($sql,4,0);
    }
    # Abfrage ob Zugang zur neuen Datenbank jetzt m�glich
    if (mysql_select_db($GUI->database->dbName,$GUI->dbConn)) {
      $debug->write("Verbindung zur MySQL Datenbank erfolgreich hergestellt.",4);
    }
    else {
      # Die neue Datenbank konnte nicht hergestellt werden
      echo 'Die Neue Datenbank konnte nicht hergestellt werden mit:';
      echo '<br>Host: '.$GUI->database->host;
      echo '<br>User: '.$GUI->database->user;
     # echo '<br>Passwd: '.$GUI->database->passwd;
      echo '<br>Datenbankname: '.$GUI->database->dbName;
      echo '<p>Das kann folgende Gr�nde haben:<lu>';
      echo '<li>Der Datenbankserver ist gerade nicht erreichbar.</li>';
      echo '<li>Die Angaben zum Host, Benutzer und Password in der config.php sind falsch.</li>';
      echo '<li>Die Angaben zum Host, Benutzer und Password in der Tabelle mysql.users sind falsch.</li>';
      echo '</lu>';
      exit;
    } # ende fehler beim aufbauen der mysql datenbank
  } # ende mysql datenbank installieren
  else {
    # Es konnte keine Datenbankverbindung aufgebaut werden
    echo 'Die Verbindung zur Kartendatenbank konnte mit folgenden Daten nicht hergestellt werden:';
    echo '<br>Host: '.$GUI->database->host;
    echo '<br>User: '.$GUI->database->user;
   # echo '<br>Passwd: '.$GUI->database->passwd;
    echo '<br>Datenbankname: '.$GUI->database->dbName;
    echo '<p>Das kann folgende Gr�nde haben:<lu>';
    echo '<li>Die Datenbank existiert noch nicht.';
    echo '<br><a href="index.php?go=install-mysql-db">Leere Datenbank jetzt anlegen</a>';
    echo '<br><a href="index.php?go=install-mysql-db&install-GUI=1">Datenbank mit Demodaten jetzt anlegen</a></li>';
    echo '<li>Der Datenbankserver ist gerade nicht erreichbar.</li>';
    echo '<li>Die Angaben zum Host, Benutzer und Password in der config.php sind falsch.</li>';
    echo '<li>Die Angaben zum Host, Benutzer und Password in der Tabelle mysql.users sind falsch.</li>';
    echo '</lu>';   
    exit;
  }
}
else {
  $debug->write("Verbindung zur MySQL Kartendatenbank erfolgreich hergestellt.",4);        
}


# Angeben, dass die Texte in latin1 zur�ckgegeben werden sollen
$GUI->database->execSQL("SET NAMES '".MYSQL_CHARSET."'",0,0);


#######################################################################
# aktuellen Benutzer abfragen
$login_name = $_SESSION['login_name'];

# �ffnen der Datenbankverbindung zur Benutzerdatenbank
if (!$userDb->open()) {
  echo 'Die Verbindung zur Benutzerdatenbank konnte mit folgenden Daten nicht hergestellt werden:';
  echo '<br>Host: '.$userDb->host;
  echo '<br>User: '.$userDb->user;
 # echo '<br>Passwd: '.$userDb->passwd;
  echo '<br>Datenbankname: '.$userDb->dbName;
  exit;
}
else {
  $debug->write("Verbindung zur MySQL Benutzerdatenbank erfolgreich hergestellt.",4);        
}
# User Daten lesen
if($_REQUEST['go'] == 'getMap_ajax'){
	$GUI->user=new user_core($login_name,0,$userDb);
}
else{
	$GUI->user=new user($login_name,0,$userDb);
}
if(BEARBEITER == 'true'){
	define('BEARBEITER_NAME', 'Bearbeiter: '.$GUI->user->Name);
}

/*
 * Eintragen eines neuen Passwortes, wenn es neu vergeben wurde
 */
if (isset($GUI->formvars['newPassword'])) {
	$GUI->Fehlermeldung=isPasswordValide($GUI->formvars['passwort'],$GUI->formvars['newPassword'],$GUI->formvars['newPassword2']);
	if ($GUI->Fehlermeldung=='') {
		$GUI->user->setNewPassword($GUI->formvars['newPassword']);
		$GUI->user->password_setting_time=date('Y-m-d H:i:s',time());
		$GUI->Fehlermeldung='Password ist erfolgreich ge�ndert worden.';
		#$GUI->formvars['newPassword'];
	}
  else {
  	$GUI->Fehlermeldung=urlencode('<font color="red">'.$GUI->Fehlermeldung.'!<br>Vorschlag f�r ein neues Password: <b>'.createRandomPassword(8).'</b></font><br>');
	  $GUI->formvars['go']='logout';
  }
}

###################################################################################
# Einstellung der Stellen_ID
# 1) Die Variable Stelle_ID wurde nicht neu gesetzt,
#    Die zuletzt genutzte Stellen_ID wird aus der Datenbank gelesen und verwendet
#    sollte dabei ein Fehler auftreten oder keine Zahl > 0 enthalten sein wird der
#    in der Konstante DEFAULTSTELLE gesetzte Wert verwendet.
# 2) Stellen_ID ist neu gesetzt worden, ein Stellenwechsel wird durchgef�hrt
#    Ist user dazu berechtigt, wird diese neue Stelle in Datenbank eingetragen,
#    sonst wird wieder alte Stelle f�r Stelle_ID verwendet und das Formular zur
#    Stellenauswahl mit Fehlermeldung angezeigt.

# zuletzt verwendete Stellen_ID f�r user aus Datenbank abfragen
$alteStelle=$GUI->user->getLastStelle();
$neueStelle=$GUI->formvars['Stelle_ID'];
if ($alteStelle==0 OR $alteStelle=='') { $alteStelle==DEFAULTSTELLE; }
# Abfragen, ob Stelle_ID in Formular neu gesetzt wurde
if ($neueStelle>0 AND $GUI->formvars['go']!='Abbrechen') {
  # Stellen_ID wurde in Formular neu ausgew�hlt deshalb versuchen in die Stelle zu wechseln
  if ($GUI->user->StellenZugriff($neueStelle)>0 OR $neueStelle==DEFAULTSTELLE) {
    # Nutzer darf laut Zuordnung zu den Stellen in die gew�nschte Stelle wechseln
    # Setzen der Stellen_ID als zuletzt benutzt
    if ($GUI->user->setStelle($neueStelle,$GUI->formvars)) {
      $Stelle_ID=$neueStelle;
    }
    else {
      $Stelle_ID=$alteStelle;
      $GUI->Fehlermeldung='Fehler beim Wechseln der Stelle. Pr�fen Sie die Angaben.';
      if($GUI->formvars['go'] == 'OWS'){
        $GUI->formvars['go_plus'] = 'Exception';
      }
      else{
        $GUI->formvars['go']='Stelle W�hlen';
      }
    }
  }
  else {
    # Nutzer ist nicht berechtigt in die gew�nschte Stelle zu wechseln
    $Stelle_ID=$alteStelle;
    $GUI->Fehlermeldung='Sie haben keine Berechtigung zum Zugriff auf die gew�hlte neue Stelle.';
    if($GUI->formvars['go'] == 'OWS'){
      $GUI->formvars['go_plus'] = 'Exception';
    }
    else{
      $GUI->formvars['go']='Stelle W�hlen';
    }
  }
}
else {
  # Stelle_ID wurde nicht in Formular neu ausgew�hlt,
  # zuletzt verwendete Stelle des Nutzers verwenden
  $Stelle_ID=$alteStelle;
}

# Erzeugen eines Stellenobjektes
if($_REQUEST['go'] == 'getMap_ajax'){
	$GUI->Stelle=new stelle_core($Stelle_ID,$userDb);
}
else{
	$GUI->Stelle=new stelle($Stelle_ID,$userDb);
}

# Pr�fung ob Client-IP-Adressen nach Vorgabe aus der Configurationsdatei �berhaupt gepr�ft werden sollen
if (CHECK_CLIENT_IP) {
	$GUI->debug->write('<br>Es wird gepr�ft ob IP-Adresspr�fung in der Stelle durchgef�hrt werden muss.',4);
	#echo 'Es wird gepr�ft ob IP-Adresspr�fung in der Stelle durchgef�rht werden muss.';
	# Pr�fen ob IP in dieser Stelle gepr�ft werden muss
	if ($GUI->Stelle->checkClientIpIsOn()) {
		#echo '<br>IP-Adresse des Clients wird in dieser Stelle gepr�ft.';
    $GUI->debug->write('<br>IP-Adresse des Clients wird in dieser Stelle gepr�ft.',4);
		# Remote_Address mit ips des Users vergleichen
	  if ($GUI->user->clientIpIsValide(getenv('REMOTE_ADDR'))==false) {
	  	# Remote_Addr stimmt nicht mit den ips des Users �berein
	  	# bzw. ist nicht innerhalb eines angegebenen Subnetzes
	    # Nutzer ist nicht berechtigt in die gew�nschte Stelle zu wechseln
	    $Stelle_ID=$alteStelle;
      $GUI->Stelle=new stelle($Stelle_ID,$userDb);
	    $GUI->Fehlermeldung='Sie haben keine Berechtigung von dem Rechner mit der IP: '.getenv('REMOTE_ADDR'). ' auf die Stelle zuzugreifen.';
	    if($GUI->formvars['go'] == 'OWS'){
	      $GUI->formvars['go_plus'] = 'Exception';
	    }
	    else{
	      $GUI->formvars['go']='Stelle W�hlen';
	    }
		}
	} # end of IP-Adressen werden in der Stelle gepr�ft
} # End of IP-Adressenpr�fung verf�gbar

# P�fung ob das Alter der Passw�rter in der Stelle gepr�ft werden m�ssen
if ($GUI->Stelle->checkPasswordAge==true) {
	# Das Alter des Passwortes des Nutzers mu� gepr�ft werden
	$remainingDays=checkPasswordAge($GUI->user->password_setting_time,$GUI->Stelle->allowedPasswordAge);
	#echo 'Verbleibende Tage '.$remainingDays;
	if ($remainingDays<=0) {
		# Der Geltungszeitraum des Passwortes ist abgelaufen
    $GUI->Fehlermeldung.='Das Passwort des Nutzers '.$GUI->user->login_name.' ist in der Stelle '.$GUI->Stelle->Bezeichnung.' abgelaufen. Passw�rter haben in dieser Stelle nur eine G�tligkeit von '.$GUI->Stelle->allowedPasswordAge.' Monaten. Geben Sie ein neues Passwort ein und notieren Sie es sich.';
    if($GUI->formvars['go'] == 'OWS'){
    	 $GUI->Fehlermeldung.=' Melden Sie sich unter '.URL.' mit Ihrem alten Password an. Daraufhin werden Sie aufgefordert ein neues Passwort einzugeben. Ist dies erfolgt, k�nnen Sie diesen Dienst weiter nutzen.';
      $GUI->formvars['go_plus'] = 'Exception';
    }
    else{
    	# Setzen eines zuf�lligen Passwortes
    	$newPassword='xxx';
    	$GUI->formvars['go']='logout';
    }
  }
}

# Abfragen der Einstellungen des Benutzers in der ausgew�hlten Stelle
# Rollendaten zuweisen
$GUI->user->setRolle($Stelle_ID);
#echo 'In der Rolle eingestellte Sprache: '.$GUI->user->rolle->language.' CharSet: '.$GUI->user->rolle->charset;
# Rollenbezogene Stellendaten zuweisen
$GUI->loadMultiLingualText($GUI->user->rolle->language,$GUI->user->rolle->charset);

##############################################################################
# �bergeben der Datenbank f�r die raumbezogenen Daten (PostgreSQL mit PostGIS)
if ($pgdbname=='') {
  # pgdbname ist leer, die Informationen zur Verbindung mit der PostGIS Datenbank
  # mit Geometriedaten werden aus der Tabelle stelle
  # der kvwmap-Datenbank $GUI->database gelesen
  if($_REQUEST['go'] == 'getMap_ajax'){
  	$PostGISdb=new pgdatabase_core();
  }
  else{
  	$PostGISdb=new pgdatabase();
  }
  $PostGISdb->host=$GUI->Stelle->pgdbhost;
  $PostGISdb->dbName=$GUI->Stelle->pgdbname;
  $PostGISdb->user=$GUI->Stelle->pgdbuser;
  $PostGISdb->passwd=$GUI->Stelle->pgdbpasswd;
}
if ($PostGISdb->dbName!='') {
  # �bergeben der GIS-Datenbank f�r GIS-Daten an die GUI
  $GUI->pgdatabase=$PostGISdb;
  # �bergeben der GIS-Datenbank f�r die Bauaktendaten an die GUI
  $GUI->baudatabase=$PostGISdb;
  
  if (!$GUI->pgdatabase->open()) {
    echo 'Die Verbindung zur PostGIS-Datenbank konnte mit folgenden Daten nicht hergestellt werden:';
    echo '<br>Host: '.$GUI->pgdatabase->host;
    echo '<br>User: '.$GUI->pgdatabase->user;
   # echo '<br>Passwd: '.$GUI->database->passwd;
    echo '<br>Datenbankname: '.$GUI->pgdatabase->dbName;
    exit;
  }
  else {
    $debug->write("Verbindung zur PostGIS Datenbank erfolgreich hergestellt.",4);
    $GUI->pgdatabase->setClientEncoding();
  }
}

####################################################################################
# �bergeben der ALKIS Datenbank f�r die raumbezogenen Daten (PostgreSQL mit PostGIS)
# Version 1.6.5
if ($ALKISdb->dbName!='') {
  $GUI->ALKISdb=$ALKISdb;
	if (!$GUI->ALKISdb->open()) {
		echo 'Die Verbindung zur ALKIS-Datenbank konnte mit folgenden Daten nicht hergestellt werden:';
		echo '<br>Host: '.$GUI->ALKISdb->host;
		echo '<br>User: '.$GUI->ALKISdb->user;
    # echo '<br>Passwd: '.$GUI->ALKISdb->passwd;
		echo '<br>Datenbankname: '.$GUI->ALKISdb->dbName;
		exit;
	}
	else {
	  $debug->write("Verbindung zur ALKIS-Datenbank erfolgreich hergestellt.",4);        
	}
}

##############################################################
# �bergeben der Gazetteer Datenbank (PostgreSQL mit PostGIS)
# Version 1.6.6
if ($Gazdb->dbName!='') {
	$GUI->Gazdb=$Gazdb;
	if (!$GUI->Gazdb->open()) {
		echo 'Die Verbindung zur Gazetteer-Datenbank konnte mit folgenden Daten nicht hergestellt werden:';
		echo '<br>Host: '.$GUI->Gazdb->host;
		echo '<br>User: '.$GUI->Gazdb->user;
		echo '<br>Datenbankname: '.$GUI->Gazdb->dbName;
		exit;
	}
	else {
	  $debug->write("Verbindung zur Gazetteers-Datenbank erfolgreich hergestellt.",4);        
	}
}

# Ausgabe der Zugriffsinformationen in debug-Datei
$debug->write('User: '.$GUI->user->login_name,4);
$debug->write('Name: '.$GUI->user->Name.' '.$GUI->user->Vorname,4);
$debug->write('Stelle_ID: '.$GUI->Stelle->id,4);
$debug->write('Stellenbezeichnung: '.$GUI->Stelle->Bezeichnung,4);
$debug->write('Host_ID: '.getenv("REMOTE_ADDR"),4); 

# Umrechnen der f�r die Stelle eingetragenen Koordinaten in das aktuelle System der Rolle
# wenn die EPSG-Codes voneinander abweichen
if ($GUI->Stelle->epsg_code!=$GUI->user->rolle->epsg_code) {
  # Umrechnen der maximalen Kartenausdehnung der Stelle
  $projFROM = ms_newprojectionobj("init=epsg:".$GUI->Stelle->epsg_code);
  $projTO = ms_newprojectionobj("init=epsg:".$GUI->user->rolle->epsg_code);
  $GUI->Stelle->MaxGeorefExt->project($projFROM, $projTO);
}

if($_SESSION['login_routines'] == true){
# hier befinden sich Routinen, die beim einloggen des Nutzers einmalig durchgef�hrt werden
	# L�schen der Rollenlayer
	if(DELETE_ROLLENLAYER == 'true'){
		$mapdb = new db_mapObj($GUI->Stelle->id, $GUI->user->id);
		$rollenlayerset = $mapdb->read_RollenLayer(NULL, 'search');
    for($i = 0; $i < count($rollenlayerset); $i++){   
      $mapdb->deleteRollenLayer($rollenlayerset[$i]['id']);
      # auch die Klassen und styles l�schen
      foreach($rollenlayerset[$i]['Class'] as $class){
        $mapdb->delete_Class($class['Class_ID']);
        foreach($class['Style'] as $style){
          $mapdb->delete_Style($style['Style_ID']);
        }
      }
    }
	}
	$_SESSION['login_routines'] = false;
}

if(isset($_FILES)) {
	foreach ($_FILES AS $datei) {
		$name = strtolower(basename($datei['name']));
		if(strpos($name,'.php') OR strpos($name,'.phtml') OR strpos($name,'.php3')) {
			echo 'PHP Dateien d�rfen nicht hochgeladen werden. Auch nicht '.$datei['name'];
			move_uploaded_file($datei['tmp_name'],LOGPATH.'AusfuehrbareDatei_vom'.date('d.m.Y',time()).'_stelleID'.$GUI->Stelle->id.'_userID'.$GUI->user->id.'_'.$datei['name'].'.txt');
			unset($_FILES);
			exit;
		}
	}
}

?>