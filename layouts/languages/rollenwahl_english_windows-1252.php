<?php
  $strCharSet="Character Set";
  $strLanguage="Language";
  $strEnglish="English";
  $strGerman="German";
  $strGUI="Graphical User Interface";
  $strButtons="Buttons";
  $strMapExtent="Map Extent";
  $strMapProjection="1.Map Projection (EPSG-Code)";
  $strSecondMapProjection="2.Map Projection (EPSG-Code)";
  $strMapSize="Map Size";
  $strTask="Task";
  $strTitleRoleSelection="Role Selection";
  $strVietnamese="Vietnamese";
  $strZoomFactor="Zoom Factor";
  $strButtonNext="Next";
  $strFontSizeGLE="GLE-Fontsize";
  $strHighlight="Tooltip-Highlighting";
  $strPlatt="Lower German";
  $strSearchColor="Result-Color";
  $strPolish="Polish";
  $strCoordType="Coordinate Presentation";
  
?>  