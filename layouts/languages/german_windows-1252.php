<?php
  $this->strID="ID";
  $this->strChoose="Bitte Wählen";
  $this->strChange="Ändern";
  $this->strDelete="Löschen";
  $this->strDeleteWarningMessage="Wollen Sie wirklich Löschen?";
  $this->strLayer="Layer";
  $this->strName="Name";
  $this->strGroup="Gruppe";
  $this->strNo="Nein";
  $this->strPleaseSelect="Bitte Auswählen";
  $this->strSave="Speichern";
  $this->strTask="Stelle";
  $this->strYes="Ja";
  $this->TaskChangeWarning="Sie sind nicht berechtigt zur Auführung der Funktion in dieser Stelle.";
  $this->strButtonBack="Zurück";  
 $this->strCancel="Abbrechen";
 $this->strSend="Senden";
 $this->strEnter="Start";
 $this->strReset="Zurücksetzen";
 $this->strSaveMapViewCommittMassage="Kartenausschnitt wurde gespeichert";
?>
