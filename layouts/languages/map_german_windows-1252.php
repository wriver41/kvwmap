<?php
  $strAvailableLayer="Verfügbare&nbsp;Themen";
  $strCoordinates="Koordinaten";
  $strShowCoordinates="Koordinatenabfrage";
  $strLoadNew="Neu Laden";
  $strMapImageURL="Bild speichern";
  $strChoose="Ausschnitt laden";
  $strMapScale="Maßstab";
  $strMapSettingsFrom="Karteneinstellungen vom";
  $strPreviousView="vorherige Ansicht";
  $strNextView="naechste Ansicht";
  $strZoomIn="Hereinzoomen";
  $strZoomOut="Herauszoomen";
  $strZoomToFullExtent="Gesamtansicht";
  $strPan="Verschieben / Pan";
  $strCoordinatesZoom="Koordinatenzoom/Abfrage";
  $strInfo="Informationsabfrage";
  $strTouchInfo="Informationsabfrage auf angrenzende Objekte";
  $strInfoWithRadius="Informationsabfrage mit Suchradius";
  $strInfoInPolygon="Informationsabfrage im Polygon";
  $strRuler="Wegstrecke messen";
  $strFreePolygon="Polygon zeichnen";
  $strFreeArrow="Pfeil zeichnen";
  $strFreeText="Text Hinzufuegen";
  $strX="R";  
  $strY="H";
  $strSave="Ausschnitt speichern";
  $strMap="Kartenausschnitt";
  $strMapSize="Größe anpassen";
  
?>
