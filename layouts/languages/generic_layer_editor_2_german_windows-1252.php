<?php
 $strEditGeom="Geometrie bearbeiten";
 $strMapZoom="Kartenausschnitt";
 $strSelectAll="alle auswählen";
 $strdelete="löschen";
 $strSelectedDatasets="ausgewählte Datensätze";
 $strCSVExport="in CSV-Datei exportieren";
 $strCSVExportAll="alle Treffer in CSV-Datei exportieren";
 $strSHPExportAll="alle Treffer in SHP-Datei exportieren";
 $strMapSelect="selektieren";
 $strzoomtodatasets="in Karte anzeigen";
 $strPrint="drucken";
 $strCreateChart="als Diagramm ausgeben";
 $strSelectAllShown="alle angezeigten auswählen";
 $strSelectThisDataset="Datensatz auswählen";
 $strNoMatch="Zu diesem Thema wurden keine Objekte gefunden!";
 $strDeleteThisDataset="Datensatz löschen";
 $strCSVExportThis="Datensatz in CSV exportieren";
 $strUKOExportThis="Geometrie ins UKO-Format exportieren";
 $strShowPK="anzeigen";
 $strNewPK="neu";
 $strShowFK="anzeigen";
 $strNewFK="neu";
 $strNewEmbeddedPK="neu";
 $strTitleGeometryEditor="Geometrie-Editor";
?>