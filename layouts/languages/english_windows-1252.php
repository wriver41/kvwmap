<?php
  $this->strID="ID";
  $this->strName="Name";
  $this->strGroup="Group";
  $this->strChoose="Choose";
  $this->strChange="Change";
  $this->strDelete="Delete";
  $this->strSave="Save";
  $this->strPleaseSelect="Please select";
  $this->strNo="No";
  $this->strYes="Yes";
  $this->strTask="Task";
  $this->strLayer="Layer";
  $this->strDeleteWarningMessage="Are you sure you want to delete?";
  $this->TaskChangeWarning="You are not allowed to use this function in the currrent task.";
  $this->strButtonBack="Back";
 $this->strCancel="Cancel";
 $this->strSend="Send";
 $this->strEnter="Enter"; 
 $this->strReset="Reset";
 $this->strSaveMapViewCommittMassage="Mapview is saved";
?>
