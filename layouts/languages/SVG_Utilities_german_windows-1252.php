<?php
  $strCornerPoint="Eckpunkte bearbeiten";
  $strCutByPolygon="Polygon ausschneiden";
  $strDrawPolygon="Polygon hinzufuegen";
  $strDeletePolygon="Polygon loeschen";
  $strDrawLine="Linie hinzufuegen";
  $strDeleteLine="alles loeschen";
  $strDelLine="Linien loeschen";
  $strSplitLine="Linien teilen";
  $strUndo="Rueckgaengig";
  $strSetPosition="Position setzen";
  $strSetGPSPosition="GPS Position setzen";
  $strRuler="Streckenmessung";
  $strBuffer="Puffer hinzufuegen";
  
?>  