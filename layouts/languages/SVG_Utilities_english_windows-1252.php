<?php
  $strCornerPoint="Edit vertices";
  $strCutByPolygon="Cut by polygon";
  $strDrawPolygon="Draw polygon";
  $strDeletePolygon="Delete polygon";
  $strSplitLine="Split Lines";
  $strUndo="Undo";
  $strSetPosition="Set Position";
  $strSetGPSPosition="Set GPS Position";
  $strRuler="Ruler";
  $strBuffer="Create Buffer";
?>  