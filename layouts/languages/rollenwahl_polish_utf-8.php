<?php
  $strCharSet="czcionka"; 
  $strLanguage="język"; 
  $strEnglish="angielski"; 
  $strGerman="niemiecki"; 
  $strGUI="graficzny interfejs u&#378;ytkownika"; 
  $strButtons="klawisze"; 
  $strMapExtent="aktualna powierzchnia map"; 
  $strMapProjection="1 odwzorowanie kartograficzne (kod EPSG)"; 
  $strSecondMapProjection="2 odwzorowanie kartograficzne (kod EPSG)"; 
  $strMapSize="wielko&#347;&#263; mapy"; 
  $strTask="miejsce"; 
  $strTitleRoleSelection="wyb&#243;r miejsca pracy"; 
  $strPolish="polska";
  $strVietnamese="wietnamski"; 
  $strZoomFactor="skala pomniejszania i powi&#281;kszania"; 
  $strButtonNext="dalej"; 
  $strFontSizeGLE="rozmiar tekstu GLE"; 
  $strHighlight="suwak jasno&#347;ci"; 
  $strPlatt="dialekt dolnoniemiecki"; 
  $strSearchColor="wynik - kolor"; 
  
  
?>