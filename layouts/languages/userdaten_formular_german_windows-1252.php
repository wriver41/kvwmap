﻿<?php
  
  $strAsteriskRequired="Werte mit * müssen eingetragen werden";
  $strDataBankID="Datenbank ID";
  $strName="Nachname*";
  $strForeName="Vorname*";
  $strLogInName="Loginname*";
  $strChangePassword="Auch Password ändern?";
  $strPassword="Password";
  $strReEnterPassword='Passwordwiederholung';
  $strAuthorizeTask="Berechtigte Stellen";
  $strTelephone="Telefon";
  $strEmail="E-Mail";
  $strButtonBack="Zurücksetzen";
  $strButtonEdit="Ändern";
  $strButtonSaveAs="Als neuen Nutzer eintragen";
  $strTitle="Benutzerdaten Editor";
  $strAllowedIps="erlaubte IP-Adressen";
?>  