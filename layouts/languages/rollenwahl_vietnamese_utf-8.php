<?php
  $strCharSet="Kiểu ký tự";
  $strLanguage="Ngôn ngữ";
  $strEnglish="Tiếng Anh";
  $strGerman="Tiếng Đức";
  $strGUI="Giao diện người dùng";
  $strButtons="Buttons";
  $strMapExtent="Phạm vi bản đồ";
  $strMapProjection="Lưới chiếu (EPSG-Code)";
  $strSecondMapProjection="2.Map Projection (EPSG-Code)";
  $strMapSize="Kích cỡ bản đồ";
  $strTask="Tác vụ";
  $strTitleRoleSelection="Lựa chọn thông số chuyển tác vụ";
  $strVietnamese="Tiếng Việt";
  $strZoomFactor="Hệ số phóng";
  $strButtonNext="Tiếp tục";
  $strFontSizeGLE="GLE-Fontsize";
  $strHighlight="Tooltip-Highlighting";
  $strPlatt="Lower German";
  $strSearchColor="Result-Color";
  $strPolish="Polish";
?>  