<?php
  $strAvailableLayer="Available&nbsp;Layer";
  $strLoadNew="Reload";
  $strCoordinates="Coordinates";
  $strShowCoordinates="Coordinates";
  $strMapImageURL="Map Image URL";
  $strMapScale="Map scale";
  $strMapSettingsFrom="Map settings from";
  $strPreviousView="Previous view";
  $strNextView="Next view";
  $strZoomIn="Zoom-in";
  $strZoomOut="Zoom-out";
  $strZoomToFullExtent="Zoom to Full Extent";
  $strPan="Pan";
  $strCoordinatesZoom="Zoom by Coordinates/Query";
  $strInfo="Info";
  $strTouchInfo="Info on surrounding objects";
  $strInfoWithRadius="Info with radius";
  $strInfoInPolygon="Info in polygon";
  $strRuler="Ruler";
  $strFreePolygon="Draw Polygon";
  $strFreeArrow="Draw Arrow";
  $strFreeText="Add Text";
  $strX="E";  
  $strY="N";
  $strChoose="Choose";
  $strSave="Speichern";
  $strMap="Map";
  
?>
