<?php
  $strCharSet="Zeichensatz";
  $strLanguage="Sprache";
  $strEnglish="Englisch";
  $strGerman="Deutsch";
  $strGUI="Graphische&nbsp;Oberfl&auml;che";
  $strButtons="Schaltflächen";
  $strMapExtent="Aktuelle Kartenausdehnung";
  $strMapProjection="1.Kartenprojektion (EPSG-Code)";
  $strSecondMapProjection="2.Kartenprojektion (EPSG-Code)";
  $strMapSize="Kartenfenstergr&ouml;&szlig;e";
  $strTask="Stelle";
  $strTitleRoleSelection="Auswahl der Arbeitsstelle";
  $strVietnamese="Vietnamesisch";
  $strZoomFactor="Zoomfaktor";
  $strButtonNext="Weiter";
  $strFontSizeGLE="GLE-Textgröße";
  $strHighlight="Tooltip-Highlighting";
  $strPlatt="Plattdeutsch";
  $strSearchColor="Suchergebnis-Farbe";
  $strPolish="Polnisch";
  $strCoordType="Koordinatendarstellung";
  
  
?>