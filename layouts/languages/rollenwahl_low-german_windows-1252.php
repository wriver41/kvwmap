<?php
  $strCharSet="Tekensatz";
  $strLanguage="Spraak";
  $strEnglish="Engelsch";
  $strGerman="Düütsch";
  $strGUI="Grafische Överflach";
  $strButtons="Schaltflächen";
  $strMapExtent="Opstunns Koortutmoot";
  $strMapProjection="1.Koortprojektion (EPSG-Code)";
  $strSecondMapProjection="2.Koortprojektion (EPSG-Code)";
  $strMapSize="Grött vun de Koortfinster";
  $strTask="Stäe";
  $strTitleRoleSelection="Utwahl vun de Arbeidsstäe";
  $strVietnamese="Vietnameesch";
  $strZoomFactor="Zoomfakter";
  $strButtonNext="Wieter";
  $strFontSizeGLE="GLE-Textgrött";
  $strHighlight="Textverkloren-Rutstrieken";
  $strPlatt="Plattdüütsch";
  $strSearchColor="Suchergebnis-Farbe";
  $strPolish="Polnisch";
?>