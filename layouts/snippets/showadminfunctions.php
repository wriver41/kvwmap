<br><h2><?php echo $this->titel; ?></h2><br>
<table border="0" cellpadding="0" cellspacing="2">
	<tr>
  	<td bordercolor="#000000" bgcolor="<?php echo BG_DEFAULT ?>">
    	<strong><font color="#000000"><a href="index.php?go=Administratorfunktionen&func=showConstants">Anzeige der Konstanten</a></font></strong>
    </td>
  </tr>
  <tr>
  	<td bordercolor="#000000" bgcolor="<?php echo BG_DEFAULT ?>">
    	<strong><font color="#000000"><a href="index.php?go=Administratorfunktionen&func=closelogfiles">Logfiles abschliessen</a></font></strong>
    </td>
  </tr>
  <tr>
    <td bordercolor="#000000" bgcolor="<?php echo BG_DEFAULT ?>"><a href="https://www.preagro.de/phpPgAdmin/redirect.php?subject=table&server=127.0.0.1%3A5432&database=kvwmapsp&schema=public&table=jagdbezirke&" target="_blank"><strong>Tabelle
      Jagdbezirke &auml;ndern</strong></a></td>
  </tr>
  <tr>
  	<td bordercolor="#000000" bgcolor="<?php echo BG_DEFAULT ?>">
    	<strong><font color="#000000"><a href="index.php?go=Administratorfunktionen&func=createRandomPassword">Erzeuge zufälliges Passwort</a></font></strong>
    </td>
  </tr>
  <tr>
    <td bordercolor="#000000" bgcolor="<?php echo BG_DEFAULT ?>">
      <strong>
        <a href="index.php?go=Administratorfunktionen&func=showStyles">
          Anzeige der bisher definierten Styles
        </a>
      </strong>
    </td>
  </tr>
  <tr>
    <td bordercolor="#000000" bgcolor="<?php echo BG_DEFAULT ?>">
      <strong>
        <a href="index.php?go=loadDenkmale_laden">
          Laden von Denkmaldaten aus HIDA XML Exportdatei
        </a>
      </strong>
    </td>
  </tr>
  <tr>
  	<td bordercolor="#000000" bgcolor="<?php echo BG_DEFAULT ?>">
    	<strong><font color="#000000"><a href="index.php?go=Administratorfunktionen&func=save_all_layer_attributes">Alle Layerattribute speichern</a></font></strong>
    </td>
  </tr>  
</table>
